#include <EEPROM.h>
#include <IRremote.h>
#define ARRAY_LENGTH( arr ) sizeof( arr ) / sizeof( arr[0] ) //Diz o tamanhos dos array

//Para Comandos via Serial
String input;
#define MAX_LENGTH 64

int Sensor_Som = 13;//D13 - Sensor de Som

int Valor_Sensor_LDR = 0;//Variavel Auxiliar para leitura da porta analogica
int Valor_Sensor_Luz = 0;//Variavel Auxiliar para leitura da porta analogica
int Valor_Sensor_Som = 0;//Variavel Auxiliar para leitura da porta analogica

#define delay1 (50)
#define delay2 (100)

typedef enum io_type { //Define enumeration como digital ou analogico intende como inteiro
    IO_TYPE_DIGITAL = 0,
    IO_TYPE_ANALOG
} io_type_t;

#define RECEPTOR_IR        (2) //D2 - Receptor Infravermelho
#define EMISSOR_IR         (3) //D3 - Emissor Infravermelho 
#define LED_TEMPO          (4) //D4 - Led Tempo
#define LED_EQUIPAMENTO    (6) //D5 - Led Equipamento
#define LED_SENSIBILIDADE (11) //D6 - Led Sensibilidade
#define LED_NV1            (7) //D7 - Led Nivel 1
#define LED_NV2            (8) //D8 - Led Nivel 2
#define LED_NV3            (9) //D9 - Led Nivel 3
#define LED_NV4           (10) //D11 - Led Nivel 4
#define LED_NV5           (12) //D12 - Led Nivel 5
#define Buzzer             (5) //D10 - Buzzer

#define Sensor_LDR     (A0) //A0 - LDR
#define Pot_Luz        (A1) //A1 - Ajuste de Luz
#define Pot_Som        (A2) //A2 - Ajuste de Som
#define Botao_Cadastro (A4) //A3 - Botão Cadastro // CH1
#define Botao_Mais     (A5) //A4 - Botão +
#define Botao_Menos    (A7) //A5 - Botão -
#define Botao_Seta     (A7) //A7 tambem // (A6) //A6 - Botão ->
#define Botao_OK       (A3) //A7 - Botão OK

#define EVT_APERT_MIN_CONT  (50) //Até 50ms nao considera aperto no botao click curto
#define EVT_SEGUR_MIN_CONT (5000)//A partir de 5S considerar aperto longo

typedef enum evento_botao {
    EVENTO_NO_PRESS = 0,
    EVENTO_BOTAO_APERT,
    EVENTO_BOTAO_SEGUR,
    EVENTO_BOTAO__NUM
} evento_botao_t;

typedef enum botao {
    BOTAO_CADASTRAR = 0,
    BOTAO_MAIS,
    BOTAO_MENOS,
    BOTAO_SETA,
    BOTAO_OK,
    BOTAO__NUM
} botao_t;

#define BOTAO_CONFIG_PORT_IDX 0 //Primeira coluna da tabela botoes_configs[]
#define BOTAO_CONFIG_MIN_IDX 1 //2 coluna da tabela botoes_configs[]
#define BOTAO_CONFIG_MAX_IDX 2 //3 coluna da tabela botoes_configs[]
#define BOTAO_CONFIG_BTN_IDX 3 //4 coluna da tabela botoes_configs[]

static uint16_t botoes_configs[][4] = { //Primeiro dimensão é a linha e a segunda é a coluna, ex: botoes_configs[0][2] = Botao_Cadastro 500.
    {Botao_Cadastro, 0,  500, BOTAO_CADASTRAR}, //BOTAO_CADASTRAR
    {Botao_Mais,     0,  500, BOTAO_MAIS}, //BOTAO_MAIS
    {Botao_Menos,    0,  500, BOTAO_MENOS}, //BOTAO_MENOS
    {Botao_Seta,   500, 1023, BOTAO_SETA}, //BOTAO_SETA
    {Botao_OK,       0,  500, BOTAO_OK}  //BOTAO_OK
};

#define SYS_IO_TYPE_IDX   0 //Segungo a tabela abaixo, define como pino digital
#define SYS_IO_NUM_IDX    1 //determina o numero do pino
#define SYS_IO_DIR_IDX    2 // Se é entrada ou saída
#define SYS_IO_TEST_T_IDX 3 // tempo de delay para acender os leds, na função de teste de i/o.

static uint16_t sys_ios[][4] = {
    {IO_TYPE_DIGITAL, RECEPTOR_IR,       INPUT,  delay1}, 
//    {IO_TYPE_DIGITAL, EMISSOR_IR,        OUTPUT, delay1*9}, 
    {IO_TYPE_DIGITAL, LED_TEMPO,         OUTPUT, delay1}, 
    {IO_TYPE_DIGITAL, LED_EQUIPAMENTO,   OUTPUT, delay1}, 
    {IO_TYPE_DIGITAL, LED_SENSIBILIDADE, OUTPUT, delay1}, 
    {IO_TYPE_DIGITAL, LED_NV1,           OUTPUT, delay1}, 
    {IO_TYPE_DIGITAL, LED_NV2,           OUTPUT, delay1}, 
    {IO_TYPE_DIGITAL, LED_NV3,           OUTPUT, delay1}, 
    {IO_TYPE_DIGITAL, LED_NV4,           OUTPUT, delay1}, 
    {IO_TYPE_DIGITAL, LED_NV5,           OUTPUT, delay1}, 
    {IO_TYPE_DIGITAL, Buzzer,            OUTPUT, delay2}
};

/* Functions Declarations */
/*    Used input Related */
evento_botao_t teste_botoes(botao_t *botao); //Declara uma função do tipo evento_botao_t, com o nome de teste_botoes e recebe um parametro chamdo botao que é um ponteiro para um botao_t. 
/*    EEPROM Related */
void zero_eeprom();
int16_t write_ir_signal(decode_results *ir_signal, uint8_t slot_idx); //int16_t = SEMPRE inteiro de 16 bits signed(-8bits a +8bits) / uint8_t = SEMPRE um Inteiro de 8 bits SEMPRE POSITIVO 0 ~ 255).
int16_t read_ir_signal(decode_results *ir_signal, uint8_t slot_idx);
/*    IR Related */
void dump(decode_results *results);
static inline uint8_t ir_prot_get_idx(decode_type_t proto);
/* Private variables */
/*    Used input Related */
static botao_t curbtn;
static const char *botoes_labels[] = {
    "Cadastro",
    "Mais",
    "Menos",
    "Seta",
    "OK"
};
static evento_botao_t curbtn_evt;
static const char *eventos_labels[] = {
    "nao apertado",
    "pressionado",
    "segurado"
};
/*    EEPROM Related */
#define EEP_LENGTH (512) // Tamanho da eeprom
#define EEP_SLOT_LENGTH (32) // Tamanho do slot de cada sinal
#define EEP_SLOT_NUM (5) // Quantidade de slot
#define EEP_SLOT_0_ADD (16) // Endereço do primeiro slot da eeprom
/*    IR Related */
static IRrecv irrecv(RECEPTOR_IR);
static decode_results results;
static IRsend irsend;
#define ir_prot_enum_offset (1)
static const char *ir_protocols_str[] = {
    "UNKNOWN",
    "UNUSED",
    "RC5",
    "RC6",
    "NEC",
    "SONY",
    "PANASONIC",
    "JVC",
    "SAMSUNG",
    "WHYNTER",
    "AIWA_RC_T501",
    "LG",
    "SANYO",
    "MITSUBISHI",
    "DISH",
    "SHARP",
    "DENON",
    "PRONTO",
    "LEGO_PF",
};
#define IR_PROT_IMPL_STS_DEC_IDX (0)
#define IR_PROT_IMPL_STS_ENC_IDX (1)
static uint8_t ir_prot_impl_status[][2] = {
    {0, 0},//UNKNOWN
    {0, 0},//UNUSED
    {DECODE_RC5, SEND_RC5},
    {DECODE_RC6, SEND_RC6},
    {DECODE_NEC, SEND_NEC},
    {DECODE_SONY, SEND_SONY},
    {DECODE_PANASONIC, SEND_PANASONIC},
    {DECODE_JVC, SEND_JVC},
    {DECODE_SAMSUNG, SEND_SAMSUNG},
    {DECODE_WHYNTER, SEND_WHYNTER},
    {DECODE_AIWA_RC_T501, SEND_AIWA_RC_T501},
    {DECODE_LG, SEND_LG},
    {DECODE_SANYO, SEND_SANYO},
    {DECODE_MITSUBISHI, SEND_MITSUBISHI},
    {DECODE_DISH, SEND_DISH},
    {DECODE_SHARP, SEND_SHARP},
    {DECODE_DENON, SEND_DENON},
    {DECODE_PRONTO, SEND_PRONTO},
    {DECODE_LEGO_PF, SEND_LEGO_PF}
};

void dump_eeprom(){
    size_t i = 0;
    while (i < EEP_LENGTH)
    {
        Serial.print(EEPROM.read(i), HEX);
        Serial.print(", ");
        i++;
        if(i % 16 == 0){ //%(Módulo, resto da divisao inteira)Imprime uma linha nova, toda vez que o indice atual for multiplo inteiro de 16.
            Serial.println("");
        }
    }
}

void zero_eeprom(){ // Zera a eeprom
    size_t i = 0; //size_t é utilidado para contar indices de uym array.
    while (i < EEP_LENGTH)
    {
        EEPROM.write(i, 0);
        i++;
    }
}

static inline uint8_t ir_prot_get_idx(decode_type_t proto){
    return proto+ir_prot_enum_offset;
}
//Grava o sinal decodificado pela LIB IRmote na EEPROM
int16_t write_ir_signal(decode_results *ir_signal, uint8_t slot_idx){ // ir_signal = objeto da classe decode_results, slot_idx = numero do slot da eeprom que o objeto vai ser guardado
    size_t current_addr = EEP_SLOT_0_ADD;
    int16_t written = 0;
    if(slot_idx >= EEP_SLOT_NUM){
        return -1;
    }
    current_addr += (slot_idx * EEP_SLOT_LENGTH);
    if((current_addr + EEP_SLOT_LENGTH) >= EEP_LENGTH){
        return -1;
    }
    EEPROM.write( current_addr, (uint8_t) ir_signal->decode_type );
    current_addr++; written++;

    EEPROM.write( current_addr,  ir_signal->bits & 0xff );
    current_addr++; written++;

    EEPROM.write( current_addr, (ir_signal->value >> 24) & 0xff );
    current_addr++; written++;
    EEPROM.write( current_addr, (ir_signal->value >> 16) & 0xff );
    current_addr++; written++;
    EEPROM.write( current_addr, (ir_signal->value >>  8) & 0xff );
    current_addr++; written++;
    EEPROM.write( current_addr,  ir_signal->value        & 0xff );
    current_addr++; written++;
    return (int16_t) written;
}
//Ler o sinal decodificado pela LIB IRmote na EEPROM
int16_t read_ir_signal(decode_results *ir_signal, uint8_t slot_idx){
    size_t current_addr = EEP_SLOT_0_ADD;
    int16_t readed = 0;
    uint8_t eep_data;
    uint8_t offset;
    size_t i;
    if(slot_idx >= EEP_SLOT_NUM){
        return -1;
    }
    current_addr += (slot_idx * EEP_SLOT_LENGTH);
    if((current_addr + EEP_SLOT_LENGTH) >= EEP_LENGTH){
        return -1;
    }

    ir_signal->decode_type = EEPROM.read( current_addr );
    current_addr++; readed++;

    ir_signal->bits = EEPROM.read( current_addr );
    current_addr++; readed++;

    ir_signal->value = 0;
    Serial.print("Initual Value: ");    
    Serial.println(ir_signal->value, HEX);    
    Serial.println("##### READING EEPROM #####");
    for(i = 0; i < 4; i++){
        offset = 24 - (i*8);
        eep_data = EEPROM.read( current_addr );
        ir_signal->value |= ((uint32_t) eep_data) << offset;
        Serial.print("ADDR: ");    
        Serial.print(current_addr, HEX);    
        Serial.print("; Value: ");    
        Serial.print(eep_data, HEX);    
        Serial.print("; Offset: ");    
        Serial.print(offset, DEC);    
        Serial.print("; Total: ");    
        Serial.println(ir_signal->value, HEX);    
        current_addr++; readed++;
    }

    return (int16_t) readed;
}

void setup() {                
    inicializar_pinos();//Configura os pinos como entrada e saída
    inicializar_serial();//Inicializa Serial e exibe informações da placa
    curbtn_evt = teste_botoes(&curbtn);//Função que testa os botões e leds da placa.
    if (curbtn_evt != EVENTO_NO_PRESS && curbtn == BOTAO_CADASTRAR){ // Se segurar o botao cadastrar no boot do equipamento zera a eeprom
        beep();
        Serial.println("zerando EEPROM");
        zero_eeprom();
    }
    irrecv.enableIRIn(); // Start the receiver Inicializa a biblioteca de recepção
    // teste_saidas();//Função que testa dos Leds e Buzzer 
}

void enviaIR(decode_results *ir_signal){
    Serial.print("##### Sending #####");
    if (!ir_prot_impl_status[ir_signal->decode_type][IR_PROT_IMPL_STS_ENC_IDX]) {
        Serial.print("Error: Protocol ");
        Serial.print(ir_protocols_str[ir_prot_get_idx(ir_signal->decode_type)]);
        Serial.println("encode is not implemented.");
        return;
    }
    if (ir_signal->decode_type == RC5) {
        irsend.sendRC5(ir_signal->value, ir_signal->bits) ;
    } else if (ir_signal->decode_type == RC6) {
        irsend.sendRC6(ir_signal->value, ir_signal->bits) ;
    } else if (ir_signal->decode_type == NEC) {
        irsend.sendNEC(ir_signal->value, ir_signal->bits) ;
    } else if (ir_signal->decode_type == SONY) {
        irsend.sendSony(ir_signal->value, ir_signal->bits) ;
    } else if (ir_signal->decode_type == PANASONIC) {
        Serial.print("Sending PANASONIC - Address: ");
        Serial.print(ir_signal->address, HEX);
        Serial.print(" Value: ");
        irsend.sendPanasonic(ir_signal->address,  ir_signal->value) ;
    } else if (ir_signal->decode_type == JVC) {
        irsend.sendJVC(ir_signal->value, ir_signal->bits,  false) ;
    } else if (ir_signal->decode_type == SAMSUNG) {
        irsend.sendSAMSUNG(ir_signal->value, ir_signal->bits) ;
    } else if (ir_signal->decode_type == WHYNTER) {
        irsend.sendWhynter(ir_signal->value, ir_signal->bits) ;
    // } else if (ir_signal->decode_type == AIWA_RC_T501) {
    //     irsend.sendAiwaRCT501(int code) ;
    // }
    } else if (ir_signal->decode_type == LG) {
        irsend.sendLG(ir_signal->value, ir_signal->bits) ;
    // }
    //     irsend.sendSanyo( ) ; // NOT WRITTEN
    // }
    //     irsend.sendMitsubishi( ) ; // NOT WRITTEN
    // }
    } else if (ir_signal->decode_type == DISH) {
        irsend.sendDISH(ir_signal->value, ir_signal->bits) ;
    } else if (ir_signal->decode_type == SHARP) {
        Serial.print("Sending PANASONIC - Address: ");
        Serial.print(ir_signal->address, HEX);
        Serial.print(" Value: ");
        irsend.sendPanasonic(ir_signal->address,  ir_signal->value) ;
    // }
    } else if (ir_signal->decode_type == DENON) {
         irsend.sendDenon(ir_signal->value, ir_signal->bits) ;
    // }
    //     irsend.sendPronto(char* code,  bool repeat,  bool fallback) ;
    // }
    //     irsend.sendLegoPowerFunctions(uint16_t data, bool repeat = true) ;
    } else {
        Serial.print("Error!!!");
    }
    return;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static int int_ret;
void loop() {
    curbtn_evt = teste_botoes(&curbtn);//Função que testa os botões e leds da placa.
    if (curbtn_evt != EVENTO_NO_PRESS){
        Serial.print("Botao ");
        Serial.print(botoes_labels[curbtn]);
        Serial.print(" ");
        Serial.print(eventos_labels[curbtn_evt]);
        Serial.println("!");
        if(curbtn >= 0 &&  curbtn < EEP_SLOT_NUM){
            if (curbtn_evt == EVENTO_BOTAO_SEGUR){
                Serial.println("#### Capturando ####");
                do {
                    int_ret = irrecv.decode(&results);
                } while (!int_ret);
                Serial.println(results.value, HEX);
                dump(&results);
                int_ret = write_ir_signal(&results, curbtn);
                if(int_ret < 0){
                    Serial.println("Erro ao ESCREVER na EEPROM");
                }
                dump_eeprom();
                irrecv.resume(); // Receive the next value
            } else if (curbtn_evt == EVENTO_BOTAO_APERT){
                Serial.println("#### Enviando ####");
                memset(&results, 0, sizeof(results));
                int_ret = read_ir_signal(&results, curbtn);
                if(int_ret < 0){
                    Serial.println("Erro ao LER na EEPROM");
                } else {
                    dump(&results);
                    enviaIR(&results);
                }
            } else {
                Serial.println("Error");
            }
        } else {
            Serial.println("Error");
        }
    }
}

void dump(decode_results *results) {
    // Dumps out the decode_results structure.
    // Call this after IRrecv::decode()
    int count = results->rawlen;
    Serial.print("Protocol: ");
    Serial.println(ir_protocols_str[ir_prot_get_idx(results->decode_type)]);
    if (results->decode_type == PANASONIC || results->decode_type == SHARP) {
        Serial.print("Address: ");
        Serial.println(results->address, HEX);
    }

    Serial.print("Value: ");
    Serial.println(results->value, HEX);
    Serial.print("Bits: ");
    Serial.println(results->bits, DEC);
    Serial.print("Raw (");
    Serial.print(count, DEC);
    Serial.print("): ");

    for (int i = 1; i < count; i++) {
        if (i & 1) {
            Serial.print(results->rawbuf[i]*USECPERTICK, DEC);
        }
        else {
            Serial.write('-');
            Serial.print((unsigned long) results->rawbuf[i]*USECPERTICK, DEC);
        }
        Serial.print(" ");
    }
    Serial.println();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void teste_saidas(){  
    size_t i;
    for(i=0; i < ARRAY_LENGTH(sys_ios); i++){
        if(sys_ios[i][SYS_IO_TYPE_IDX] == IO_TYPE_DIGITAL &&
           sys_ios[i][SYS_IO_DIR_IDX] == OUTPUT)
        {
            digitalWrite(sys_ios[i][SYS_IO_NUM_IDX], HIGH);
            delay (sys_ios[i][SYS_IO_TEST_T_IDX]);
            digitalWrite(sys_ios[i][SYS_IO_NUM_IDX], LOW); 
        }
    }
}

void teste_entradas(){
    Valor_Sensor_LDR = analogRead(Sensor_LDR);
    Valor_Sensor_Luz = analogRead(Pot_Luz);
    Valor_Sensor_Som = analogRead(Pot_Som);
    Serial.print("Sensores LDR=");
    Serial.print(Valor_Sensor_LDR);
    delay(2); 
    Serial.print(" Potenc.Luz=");
    Serial.print(Valor_Sensor_Luz);
    delay(2);
    Serial.print(" Potenc.Som=");
    Serial.println(Valor_Sensor_Som);
    delay(2);  
    delay(1000); 
}

void inicializar_serial(){
    Serial.begin(9600);//Inicializa a comunicação Serial
    input.reserve(MAX_LENGTH + 1);  // Define o tamanho máximo do buffer (+ 1 por causa do \0 no final
    Serial.println("Freedom Engenharia\n"
    "Firmware: Desligador\n"
    "Versao:   0.1.\n\n"
    "Sistema Inicializado!\n"); 
}

void inicializar_pinos(){
    size_t i=0;
    for(i=0; i < ARRAY_LENGTH(sys_ios); i++){
        if(sys_ios[i][SYS_IO_TYPE_IDX] == IO_TYPE_DIGITAL){
            pinMode(sys_ios[i][SYS_IO_NUM_IDX], sys_ios[i][SYS_IO_DIR_IDX]);   
        }
    }
}

void beep(){
    digitalWrite(Buzzer, HIGH);
    delay(delay2);
    digitalWrite(Buzzer, LOW);
    delay(delay2);
}

evento_botao_t teste_botoes(botao_t *botao){
    size_t i;
    uint16_t analog_read_val;
    uint16_t press_count;
    evento_botao_t evt = EVENTO_NO_PRESS; 
    for(i = 0; i < BOTAO__NUM ; i++){
        analog_read_val = analogRead(botoes_configs[i][BOTAO_CONFIG_PORT_IDX]);
        press_count = 0;
        delay(2);
        while(botoes_configs[i][BOTAO_CONFIG_MIN_IDX] <= analog_read_val && 
           analog_read_val < botoes_configs[i][BOTAO_CONFIG_MAX_IDX])
        {
            press_count++;
            analog_read_val = analogRead(botoes_configs[i][BOTAO_CONFIG_PORT_IDX]);
            delay(1);
            Serial.println(press_count, DEC);
        }
        if( 0 < press_count && press_count <= EVT_APERT_MIN_CONT ){
            evt = EVENTO_BOTAO_APERT;
            *botao = (botao_t)botoes_configs[i][BOTAO_CONFIG_BTN_IDX];
            break;
        } else if( EVT_APERT_MIN_CONT < press_count && press_count <= EVT_SEGUR_MIN_CONT ){
            evt = EVENTO_BOTAO_SEGUR;
            *botao = (botao_t)botoes_configs[i][BOTAO_CONFIG_BTN_IDX];
            break;
        } else {
            continue;// FIXME ???
        }
    }
    delay(10);
    return evt; 
}

void serial_Comandos(){
    if (Serial.available()) {
        char c = Serial.read(); // Recebe um caracter
        if(c == '\n') {     // Se foi digitado um ENTER entao processa a String       
            Serial.println("A string digitada foi ");
            Serial.println(input);

            input.toUpperCase();    // Converte toda a String para maiusculo
            input.trim();           // Tira os espacos antes e depois

            if(input == "LIGA"){    
                Serial.println("Ligar LED Pino D13"); 
                digitalWrite(LED_TEMPO,HIGH);
            }
            else if (input == "DESLIGA") {
                Serial.println("Desligar LED Pino D13");
                digitalWrite(LED_TEMPO,LOW);
            }       
            input = "";// Limpa a String para comecar a armazenar de novo         
        } 
        else {    
            // Se nao veio um ENTER entao vai armazenando até o tamanho maximo que a string pode suportar        
            if(input.length() < MAX_LENGTH) {
                if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')){ // Aqui estamos filtrando so as letras. Poderia filtrar numeros e espaco por exemplo
                    input += c;
                }
            }
        }
    } 
}
